import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import {plainToClass} from "class-transformer";
import { Animal } from "../entity/Animal";
import { CreateAnimalDTO, AnimalDTO, CreateAnimalRequest } from '../models/animal';
import { Category } from "../entity/Category";
import { validate } from "class-validator";
import { AnimalUsers } from "../entity/AnimalUser";
import { PurchaseDTO } from "../models/purchases";
import { AnimalComments } from "../entity/AnimalComment";
import { CreateCommentRequest, CommentDTO } from "../models/comments";

class AnimalController {

    static getAnimals = async (req: Request, res: Response) => {
        const animalRepository = getRepository(Animal);
        const token = res.locals.jwtPayload;
        let animals: Animal[];
        try {
            animals = await animalRepository
                    .createQueryBuilder('animal')
                    .leftJoinAndSelect('animal.category', 'category')
                    .leftJoinAndSelect('animal.user', 'user')
                    .leftJoinAndSelect('animal.purchases', 'purchase', `purchase.userId = ${token.userId} and purchase.animalId = animal.id`)
                    .where('animal.isDeleted = false')
                    .orderBy('animal.createdAt', 'DESC')
                    .getMany();

            const animalsDTO = plainToClass(AnimalDTO, animals, { excludeExtraneousValues: true });
            return res.send(animalsDTO);
        } catch (err) {
            return res.status(500).send({err: 'Error Occured'});
        }
    }

    static createAnimal = async (req: Request, res: Response) => {
        const token = res.locals.jwtPayload;
        const categoryRepository = getRepository(Category);
        const userRepository = getRepository(User);
        const animalRepository = getRepository(Animal);

        const body = req.body;
        const createAnimalRequest = plainToClass(CreateAnimalRequest, body, {excludeExtraneousValues: true});

        const errors = await validate(createAnimalRequest, {validationError: {target: false, value: false}});
        if (errors && errors.length > 0) {
            return res.status(400).send(errors); 
        }

        const user = await userRepository.findOne(token.userId);
        const category = await categoryRepository.findOne(createAnimalRequest.categoryId);
        
        let animal = new Animal();
        animal.user = user;
        animal.category = category;
        animal.content = createAnimalRequest.content;
        animal.averageConsumption = createAnimalRequest.averageConsumption;
        animal.averageProduction = createAnimalRequest.averageProduction;
        animal.name = createAnimalRequest.name;
        animal.isCommentsEnabled = createAnimalRequest.isCommentsEnabled;
        try {
            animal = await animalRepository.save(animal);
        } catch (err) {
            return res.status(500).send({err: 'Error Occured'});
        }

        const newAnimal = plainToClass(CreateAnimalDTO, animal, { excludeExtraneousValues: true });

        res.send(newAnimal);
    }

    static getAnimalById = async (req: Request, res: Response) => {
        const animalId: number = req.params.id;
        const { userId } = res.locals.jwtPayload;
        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        const animalRepository = getRepository(Animal);
        try {
            const animal= await animalRepository
                    .createQueryBuilder('animal')
                    .leftJoinAndSelect('animal.category', 'category')
                    .leftJoinAndSelect('animal.user', 'user')
                    .leftJoinAndSelect('animal.purchases', 'purchase', 'purchase.userId = :userId and purchase.animalId = animal.id', {userId})
                    .where('animal.id = :animalId', {animalId})
                    .getOne();

            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${animalId}`});
            }

            const animalDTO = plainToClass(AnimalDTO, animal, {excludeExtraneousValues: true});
            return res.send(animalDTO);
        } catch (error) {
            return res.status(500).send({err: 'Error occured'});
        }
    }

    static getAnimalByCategoryName = async (req: Request, res: Response) => {
        const categoryName: string = req.params.categoryName;

        if (!categoryName) {
            return res.status(400).send({err: 'categoryName is required'});
        }

        const animalRepository = getRepository(Animal);
        try {
            const animals = await animalRepository
                    .createQueryBuilder('animal')
                    .leftJoinAndSelect('animal.category', 'category')
                    .where('category.name = :categoryName', {categoryName})
                    .execute()

            if (!animals.length) {
                return res.status(404).send({err: `Animal not found for categoryName: ${categoryName}`});
            }

            return res.send(animals);
        } catch (error) {
            return res.status(500).send({err: 'Error occurred'});
        }
    }

    static getAnimalPurchases = async (req: Request, res: Response) => {
        const animalId: number = req.params.id;

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        const animalpurchasesRepository = getRepository(AnimalUsers);
        try {
            const purchases = await animalpurchasesRepository.find({where: {animalId}, relations: ['user'], order: {createdAt: 'DESC'}});
            const purchasesDTO = plainToClass(PurchaseDTO, purchases, {excludeExtraneousValues: true});
            return res.send(purchasesDTO);
        } catch (error) {
            return res.status(500).send({err: `Error occured`});
        }
    }

    static purchaseAnimal = async (req: Request, res: Response) => {
        const { userId } = res.locals.jwtPayload;
        const animalId: string = req.params.id;

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        const animalRepository = getRepository(Animal);
        const animalPurchasesRepository = getRepository(AnimalUsers);

        try {
            const animal = await animalRepository.findOne(animalId);
            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${animalId}`});
            }

            const like = await animalPurchasesRepository.findOne({where: {userId, animalId}});
            if (like) {
                await animalPurchasesRepository.remove(like);
            } else {
                const newLike = new AnimalUsers();
                newLike.animalId = animalId;
                newLike.userId = userId;
                await animalPurchasesRepository.save(newLike);
            }
            return res.send();

        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    }

    static getAnimalComments = async (req: Request, res: Response) => {
        const animalId: string = req.params.id;

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        const animalCommentsRepository = getRepository(AnimalComments);

        try {
            const comments = await animalCommentsRepository.find({
                where: {animalId, isDeleted: false},
                relations: ['user', 'animal'],
                order: {createdAt: 'DESC'}
            });
            const commentsDTO = plainToClass(CommentDTO, comments, {excludeExtraneousValues: true});
            return res.send(commentsDTO);

        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    }

    static getCommentById = async (req: Request, res: Response) => {
        const { userId } = res.locals.jwtPayload;
        const commentId: string = req.params.commentId;
        const animalId: string = req.params.id;

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        if (!commentId) {
            return res.status(400).send({err: 'commentId is required'});
        }

        const animalCommentsRepository = getRepository(AnimalComments);
        const animalRepository = getRepository(Animal);

        try {

            const animal = await animalRepository.findOne(animalId);
            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${animalId}`});
            }

            const comment = await animalCommentsRepository.findOne(commentId, {relations: ['user', 'animal']});
            if (!comment) {
                return res.status(404).send({err: `Comment not found with id: ${commentId}`});
            }

            const commentDTO = plainToClass(CommentDTO, comment, {excludeExtraneousValues: true});
            return res.send(commentDTO);

        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    }

    static deleteComment = async (req: Request, res: Response) => {
        const { userId } = res.locals.jwtPayload;
        const commentId: string = req.params.commentId;
        const animalId: string = req.params.id;

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        if (!commentId) {
            return res.status(400).send({err: 'commentId is required'});
        }

        const animalCommentsRepository = getRepository(AnimalComments);
        const animalRepository = getRepository(Animal);

        try {

            const animal = await animalRepository.findOne(animalId);
            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${animalId}`});
            }

            const comment = await animalCommentsRepository.findOne(commentId, {relations: ['user', 'animal']});
            if (!comment) {
                return res.status(404).send({err: `Comment not found with id: ${commentId}`});
            }

            // TODO: animal.userId is deleted after business logic change. Find a way to fix it

            // if (comment.user.id !== userId || comment.animal.userId !== userId) {
            //     return res.status(403).send();
            // }

            comment.isDeleted = true;
            await animalCommentsRepository.save(comment);

            const commentDTO = plainToClass(CommentDTO, comment, {excludeExtraneousValues: true});
            return res.send(commentDTO);

        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    }

    static createComment = async (req: Request, res: Response) => {
        const { userId } = res.locals.jwtPayload;
        const animalId: string = req.params.id;
        const body = req.body;
        const createCommentRequest = plainToClass(CreateCommentRequest, body, {excludeExtraneousValues: true});

        if (!createCommentRequest.content) {
            return res.status(400).send({err: 'Content is required'});
        }

        if (!animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        const userRepository = getRepository(User);
        const animalRepository = getRepository(Animal);
        const animalCommentsRepository = getRepository(AnimalComments);

        try {
            const user = await userRepository.findOne(userId);
            const animal = await animalRepository.findOne(animalId);
            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${animalId}`});
            }

            if (!animal.isCommentsEnabled) {
                return res.status(403).send({err: `Comments are disabled for this animal`});
            }

            let comment = new AnimalComments();
            comment.content = createCommentRequest.content;
            comment.animal = animal;
            comment.animalId = animal.id;
            comment.user = user;
            await animalCommentsRepository.save(comment);

            animal.commentCount += 1;
            await animalRepository.save(animal);

            const commentDTO = plainToClass(CommentDTO, comment, {excludeExtraneousValues: true});

            return res.send(commentDTO);

        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    }

}

export default AnimalController;