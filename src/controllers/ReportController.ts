import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { AnimalReport } from "../entity/AnimalReport";
import { plainToClass } from "class-transformer";
import { CreateReportRequest, ReportDTO, ReportFilterModel } from "../models/animalReport";
import { Animal } from "../entity/Animal";
import { User } from "../entity/User";

class ReportController {
    static getAll = async (req: Request, res: Response) => {
        const filter = req.query;
        const reportFilterModel = plainToClass(ReportFilterModel, filter, {excludeExtraneousValues: true});

        let query = {};

        if (reportFilterModel.animalId) {
            query['animalId'] = reportFilterModel.animalId;
        }

        if (reportFilterModel.userId) {
            query['userId'] = reportFilterModel.userId;
        }

        const animalReportPepository = getRepository(AnimalReport);
        try {
            const reports = await animalReportPepository.find({
                relations: ['user', 'animal'],
                where: query,
                order: {createdAt: 'DESC'},
                take: reportFilterModel.take,
                skip: reportFilterModel.skip
            });
            const reportsDTO = plainToClass(ReportDTO, reports, { excludeExtraneousValues: true });
            return res.send(reportsDTO);
        } catch(err) {
            return res.status(500).send({err: `Error occured`});
        }
    };

    static createReport = async (req: Request, res: Response) => {
        const { userId } = res.locals.jwtPayload;
        const body = req.body;
        const createReportRequest = plainToClass(CreateReportRequest, body, {excludeExtraneousValues: true});

        if (!createReportRequest.animalId) {
            return res.status(400).send({err: 'animalId is required'});
        }

        try {
            const userRepository = getRepository(User);
            const animalRepository = getRepository(Animal);
            const animalReportPepository = getRepository(AnimalReport);

            const user = await userRepository.findOne(userId);
            const animal = await animalRepository.findOne(createReportRequest.animalId);
            if (!animal) {
                return res.status(404).send({err: `Animal not found with id: ${createReportRequest.animalId}`});
            }

            let report = new AnimalReport();
            report.reason = createReportRequest.reason;
            report.animal = animal;
            report.user = user;
            report = await animalReportPepository.save(report);

            const reportDTO = plainToClass(ReportDTO, report, { excludeExtraneousValues: true });
            return res.send(reportDTO);
        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    };

    static getOneById = async (req: Request, res: Response) => {
        const reportId: number = req.params.id;

        if (!reportId) {
            return res.status(400).send({err: 'reportId is required'});
        }

        try {
            const animalReportPepository = getRepository(AnimalReport);
            const report = await animalReportPepository.findOne(reportId, {relations: ['user', 'animal']});
            const reportDTO = plainToClass(ReportDTO, report, { excludeExtraneousValues: true });
            return res.send(reportDTO);
    
        } catch (err) {
            return res.status(500).send({err: `Error occured`});
        }
    };
}

export default ReportController;