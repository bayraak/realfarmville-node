import { ManyToOne, Entity, PrimaryColumn, Column, CreateDateColumn } from "typeorm";
import { User } from "./User";
import { Animal } from "./Animal";

@Entity()
export class AnimalUsers {
    @PrimaryColumn() 
    animalId: string;

    @PrimaryColumn() 
    userId: number;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(type => User, user => user.animals)
    user!: User

    @ManyToOne(type => Animal, animal => animal.users)
    animal!: Animal
}