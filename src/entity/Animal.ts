import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToMany,
    Unique,
} from "typeorm";
import { User } from "./User";
import { Category } from "./Category";
import { AnimalUsers } from "./AnimalUser";
import { AnimalComments } from "./AnimalComment";
import { AnimalReport } from "./AnimalReport";
import { MaxLength } from "class-validator";

@Entity()
export class Animal {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    content: string;
    
    @Column()
    name: string;

    @Column({type: 'decimal'})
    price: number;

    @Column({type: 'decimal'})
    predictedLifetime: number;

    @Column({type: 'decimal'})
    averageProduction: number;

    @Column({type: 'decimal'})
    averageWeight?: number;

    @MaxLength(64)
    @Column()
    origin: string;

    @Column({type: 'decimal'})
    averageConsumption: number;

    @Column({
        default: 0
    })
    purchaseCount: number;

    @Column({
        default: 0
    })
    commentCount: number;

    @Column({
        default: false
    })
    isDeleted: boolean;

    @Column({
        default: true
    })
    isCommentsEnabled: boolean;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.animals)
    user: User;

    @ManyToOne(type => Category, category => category.animals)
    category: Category;

    @OneToMany(type => AnimalUsers, animalUser => animalUser.animal)
    users!: AnimalUsers[];

    @OneToMany(type => AnimalComments, animalComment => animalComment.animal)
    comments!: AnimalComments[];

    @OneToMany(type => AnimalReport, animalReport => animalReport.animal)
    reports!: AnimalReport[];
}