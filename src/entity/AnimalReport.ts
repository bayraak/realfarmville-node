import { ManyToOne, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Animal } from "./Animal";

@Entity()
export class AnimalReport {
    @PrimaryGeneratedColumn() 
    id: number;
    
    @Column()
    reason: string;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    userId: number;

    @Column()
    animalId: string;

    @ManyToOne(type => User, user => user.reports)
    user!: User

    @ManyToOne(type => Animal, animal => animal.reports)
    animal!: Animal
}