import { ManyToOne, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Animal } from "./Animal";

@Entity()
export class AnimalComments {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    animalId: string;

    @Column()
    userId: number;
    
    @Column()
    content: string;

    @Column({ default: false })
    isDeleted: boolean;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.comments)
    user!: User

    @ManyToOne(type => Animal, animal => animal.comments)
    animal!: Animal
}