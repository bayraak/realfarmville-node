import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Unique } from "typeorm";
import { Length } from "class-validator";
import { RoleToCategory } from "./RoleToCategory";
import { Animal } from "./Animal";


@Entity()
@Unique(["name"])
export class Category {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(2, 20)
    name: string;
    
    @Column('varchar' , {default: ''})
    logoUrl: string = '';

    @OneToMany(type => RoleToCategory, roleToCategory => roleToCategory.category)
    roles!: RoleToCategory[];
    
    @OneToMany(type => Animal, animal => animal.category)
    animals: Animal[];
}