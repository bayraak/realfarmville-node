import { Expose, Type } from "class-transformer";
import { UserDTO } from "./user";

export class PurchaseDTO {
    @Expose() animalId: string;
    @Expose() userId: number;
    @Expose() createdAt: Date;
    @Expose() @Type(() => UserDTO) user: UserDTO;
}