import { Expose, Type, Transform } from "class-transformer";
import { CategoryDTO } from "./category";
import { UserDTO } from "./user";
import { MinLength, IsNotEmpty } from "class-validator";

export class CreateAnimalDTO {
    @Expose() id: string;
    @Expose() content: string;
    @Expose() purchaseCount: number;
    @Expose() commentCount: number;
    @Expose() isDeleted: boolean;
    @Expose() isCommentsEnabled: boolean;
    @Expose() createdAt: Date;
    @Expose() updatedAt: Date;
    @Expose() @Type(() => CategoryDTO) category: CategoryDTO;
}

export class CreateAnimalRequest {
    @IsNotEmpty({message: 'Content can\'t be empty'})
    @Expose()
    content: string;

    @IsNotEmpty({message: 'Name can\'t be empty'})
    @Expose()
    name: string;

    @IsNotEmpty({message: 'Predicted lifetime can\'t be empty'})
    @Expose()
    predictedLifetime: number;

    @IsNotEmpty({message: 'Average consumption can\'t be empty'})
    @Expose()
    averageConsumption: number;

    @IsNotEmpty({message: 'Average production can\'t be empty'})
    @Expose()
    averageProduction: number;

    @Expose()
    @Transform(value => value !== null ? value : true, { toClassOnly: true })
    isCommentsEnabled: boolean;

    @Expose()
    @IsNotEmpty({message: 'CategoryId can\'t be empty'})
    categoryId: number;
}

export class AnimalDTO {
    @Expose() id: string;
    @Expose() content: string;
    @Expose() purchaseCount: number;
    @Expose() commentCount: number;
    @Expose() isDeleted: boolean;
    @Expose() isCommentsEnabled: boolean;
    @Expose() createdAt: Date;
    @Expose() updatedAt: Date;
    @Expose() userId: number;
    @Expose() @Type(() => CategoryDTO) category: CategoryDTO;
    @Expose() @Type(() => UserDTO) user: UserDTO;

    @Expose() 
    @Transform((value, animal) => {
        return (animal && animal.purchases && animal.purchases.length > 0) ? true : false
    }) 
    isPurchased: boolean;
}