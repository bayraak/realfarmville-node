import { Router } from "express";
import AnimalController from "../controllers/AnimalController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

//Get all animals
router.get("/", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.getAnimals);

//Create new animal
router.post("/", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.createAnimal);

//Get animal by id
router.get("/:id", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.getAnimalById);

//Get animal by categoryName
router.get("/category/:categoryName", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.getAnimalByCategoryName);

//Get animal purchases
router.get("/:id/purchases", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.getAnimalPurchases);

//Purchase an animal
router.get("/:id/purchaseAnimal", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.purchaseAnimal);

//Create new comment
router.post("/:id/comments", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.createComment);

//Get animal comments
router.get("/:id/comments", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.getAnimalComments);

//Get single comment
router.get("/:id/comments/:commentId", [checkJwt, checkRole(["ADMIN"])], AnimalController.getCommentById);

//Delete comment
router.delete("/:id/comments/:commentId", [checkJwt, checkRole(["USER", "ADMIN"])], AnimalController.deleteComment);


export default router;