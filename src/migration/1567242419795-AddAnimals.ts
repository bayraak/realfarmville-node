import { MigrationInterface, QueryRunner, getRepository, getConnection } from "typeorm";
import { Animal } from "../entity/Animal";
import Cows from "./seed/animal/Cow";
import Horses from "./seed/animal/Horse";
import Chickens from "./seed/animal/Chicken";
import { Category } from "../entity/Category";
import Sheep from "./seed/animal/Sheep";
import BeeHives from "./seed/animal/BeeHive";

export class AddAnimals1567242419795 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const animalRepository = getRepository(Animal);

        const categorizedCows = await this.assignCategoryToAnimals(Cows, 'Cow');
        const categorizedHorses = await this.assignCategoryToAnimals(Horses, 'Horse');
        const categorizedChickens = await this.assignCategoryToAnimals(Chickens, 'Chicken');
        const categorizedSheep = await this.assignCategoryToAnimals(Sheep, 'Sheep');
        const categorizedBeeHives = await this.assignCategoryToAnimals(BeeHives, 'Bee Hive')


        const allAnimals: Animal[] = [].concat.apply([],
                                        [categorizedCows, 
                                        categorizedHorses, 
                                        categorizedChickens,
                                        categorizedSheep,
                                        categorizedBeeHives]);
        try {
            await animalRepository.save(allAnimals);
        } catch (err) {
            console.error(err);
            return;
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Animal)
            .execute();
    }

    private async assignCategoryToAnimals(animals: Animal[], categoryName: string): Promise<Animal[]> {
        const animalCategoryRepository = getRepository(Category);

        let animalCategory;
        try {
            animalCategory = await animalCategoryRepository.findOneOrFail({ where: { name: categoryName } })
        }
        catch (err) {
            console.log(`Animal category with name: ${categoryName} not found`, err);
            return animals;
        }

        animals.forEach(animal => {
            animal.category = animalCategory;
        });

        return animals;
    }
}
