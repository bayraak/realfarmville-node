import { Animal } from "../../../entity/Animal"

let HolsteinFrisian = new Animal();

HolsteinFrisian.name = "Holstein-Frisian";
HolsteinFrisian.averageProduction = 22000;
HolsteinFrisian.averageConsumption = 300;
HolsteinFrisian.origin = "Netherlands";
HolsteinFrisian.content = "What makes them “Ideal” – Holsteins excel at producing milk, and are the high performance athletes of cattle. They are unrivaled in terms of milk production volume. Their calm, relaxed nature make them easy to handle."
HolsteinFrisian.predictedLifetime = 2190 // 6 years in days 
HolsteinFrisian.averageWeight = 620 // in kg
HolsteinFrisian.price = 770;

let JerseyCattle = new Animal();

JerseyCattle.name = "Jersey Cattle";
JerseyCattle.averageProduction = 16500;
JerseyCattle.averageConsumption = 180;
JerseyCattle.origin = "Jersey Island";
JerseyCattle.content = "What makes them “Ideal” – Brown Swiss are strong, hardy cows. It is this strength and power that sets them apart from other dairy breeds. Being the biggest dairy breed, their durability and longevity are great characteristics."
JerseyCattle.predictedLifetime = 4015 // 11 years in days 
JerseyCattle.averageWeight = 450 // in kg
JerseyCattle.price = 820;

let Guernsey = new Animal();

Guernsey.name = "Guernsey";
Guernsey.averageProduction = 16500;
Guernsey.averageConsumption = 220;
Guernsey.origin = "Isle of Guernsey";
Guernsey.content = "Guernsey Cows are renowned for their special milk which is golden in color. The milks golden color is due to large amounts of beta-carotene which is a source of Vitamin A. Beta-carotene has been found to reduce the risk of certain types of cancer."
Guernsey.predictedLifetime = 4380 // 12 years in days 
Guernsey.averageWeight = 520 // in kg
Guernsey.price = 940;

let BrownSwiss = new Animal();

BrownSwiss.name = "Brown Swiss";
BrownSwiss.averageProduction = 18500;
BrownSwiss.averageConsumption = 220;
BrownSwiss.origin = "Switzerland";
BrownSwiss.content = "Brown Swiss are strong, hardy cows. It is this strength and power that sets them apart from other dairy breeds. Being the biggest dairy breed, their durability and longevity are great characteristics."
BrownSwiss.predictedLifetime = 3650 // 10 years in days 
BrownSwiss.averageWeight = 680 // in kg
BrownSwiss.price = 1100;

let Ayrshire = new Animal();

Ayrshire.name = "Ayrshire";
Ayrshire.averageProduction = 18000;
Ayrshire.averageConsumption = 230;
Ayrshire.origin = "Scotland";
Ayrshire.content = "The Ayrshire originated in Scotland where they were specially bred with animals being brought in from the Netherlands. These animals were then exported around the world to many different locations around the globe."
Ayrshire.predictedLifetime = 3650 // 10 years in days 
Ayrshire.averageWeight = 550 // in kg
Ayrshire.price = 960;

let MilkingShorthorn = new Animal();

MilkingShorthorn.name = "Ayrshire";
MilkingShorthorn.averageProduction = 17500;
MilkingShorthorn.averageConsumption = 230;
MilkingShorthorn.origin = "Scotland";
MilkingShorthorn.content = "The Ayrshire originated in Scotland where they were specially bred with animals being brought in from the Netherlands. These animals were then exported around the world to many different locations around the globe."
MilkingShorthorn.predictedLifetime = 3285 // 9 years in days
MilkingShorthorn.averageWeight = 600 // in kg 
MilkingShorthorn.price = 820;

const Cows: Animal[] = [HolsteinFrisian, JerseyCattle, Guernsey, Ayrshire, BrownSwiss, MilkingShorthorn];

export default Cows;