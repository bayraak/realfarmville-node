import { Animal } from "../../../entity/Animal"

let ArabianHorse = new Animal();

ArabianHorse.name = "Arabian ";
ArabianHorse.averageProduction = 0;
ArabianHorse.averageConsumption = 300;
ArabianHorse.origin = "Saudi Arabia";
ArabianHorse.content = "The Arabian horse has long been a favorite the world over. Hailing from the Arabian Peninsula, this breed is easy to spot with its distinctive head shape and high, proud tail carriage."
ArabianHorse.predictedLifetime = 9125 // 25 years in days
ArabianHorse.averageWeight = 450 // in kg 
ArabianHorse.price = 22000;

let TennesseeWalker = new Animal();

TennesseeWalker.name = "Tennessee Walker";
TennesseeWalker.averageProduction = 0;
TennesseeWalker.averageConsumption = 350;
TennesseeWalker.origin = "Southern United States ";
TennesseeWalker.content = "In fact, it is believed that Robert E. Lee's mount, Traveler, was part Tennessee Walking Horse. Today, Tennessee Walkers are used as both show horses and pleasure mounts."
TennesseeWalker.predictedLifetime = 10950 // 30 years in days 
TennesseeWalker.averageWeight = 500 // in kg
TennesseeWalker.price = 820;

const Horses: Animal[] = [ArabianHorse, TennesseeWalker];

export default Horses;