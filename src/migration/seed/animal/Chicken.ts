import { Animal } from "../../../entity/Animal"

let Ameraucana = new Animal();

Ameraucana.name = "Ameraucana";
Ameraucana.averageProduction = 150; // piece in year
Ameraucana.averageConsumption = 5;
Ameraucana.origin = "South America – Chile";
Ameraucana.content = "Light Blue Eggs"
Ameraucana.predictedLifetime = 2737.5 // 7.5 years in days
Ameraucana.averageWeight = 0.8 // in kg 
Ameraucana.price = 18; // in euros

let BelgianDuccle = new Animal();

BelgianDuccle.name = "Belgian d’Uccle";
BelgianDuccle.averageProduction = 100;
BelgianDuccle.averageConsumption = 4;
BelgianDuccle.origin = "Belgium";
BelgianDuccle.content = "Cream Eggs"
BelgianDuccle.predictedLifetime = 2555 // 7 years in days 
BelgianDuccle.averageWeight = 0.75 // in kg
BelgianDuccle.price = 13;

let Andalusian = new Animal();

Andalusian.name = "Tennessee Walker";
Andalusian.averageProduction = 150;
Andalusian.averageConsumption = 5   ;
Andalusian.origin = "Spain";
Andalusian.content = "White Eggs"
Andalusian.predictedLifetime = 2190 // 6 years in days 
Andalusian.averageWeight = 0.9 // in kg
Andalusian.price = 16;

const Chickens: Animal[] = [Ameraucana, Andalusian, BelgianDuccle];

export default Chickens;