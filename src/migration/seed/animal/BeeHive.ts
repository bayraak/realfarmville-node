import { Animal } from "../../../entity/Animal"

let Carniolan = new Animal();

Carniolan.name = "Carniolan";
Carniolan.averageProduction = 45; // kg per colony
Carniolan.averageConsumption = 2;
Carniolan.origin = "Balkans";
Carniolan.content = "Darker Golden Honey"
Carniolan.predictedLifetime = 7300 // 20 years in days
Carniolan.averageWeight = 10 // in kg 
Carniolan.price = 200; // in euros

let Italian = new Animal();

Italian.name = "Italian";
Italian.averageProduction = 35; // kg per colony
Italian.averageConsumption = 3;
Italian.origin = "Balkans";
Italian.content = "Golden Honey"
Italian.predictedLifetime = 5475 // 15 years in days
Italian.averageWeight = 10 // in kg 
Italian.price = 300; // in euros


const BeeHives: Animal[] = [Carniolan, Italian];

export default BeeHives;