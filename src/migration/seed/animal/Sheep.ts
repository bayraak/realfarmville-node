import { Animal } from "../../../entity/Animal"

let Karakachan = new Animal();

Karakachan.name = "Karakachan";
Karakachan.averageProduction = 25; // milk in kg
Karakachan.averageConsumption = 10;
Karakachan.origin = "Balkans - Macedonia";
Karakachan.content = `Officially recognized.
Area of distribution several countries of the Balkans; in Macedonia around Shtip, Strumica; Veles, Bitola
Black to dark brown color, sometimes also white varieties occur.`
Karakachan.predictedLifetime = 1825 // 5 years in days
Karakachan.averageWeight = 45 // in kg
Karakachan.price = 80;

let Ovchepolian = new Animal();

Ovchepolian.name = "Ovchepolian";
Ovchepolian.averageProduction = 70;
Ovchepolian.averageConsumption = 15;
Ovchepolian.origin = "Macedonia - Ovchepolian Plateau";
Ovchepolian.content = "Wool: 1.2 - 1.7 kg; Fat 5.41%;"
Ovchepolian.predictedLifetime = 1642.5 // 4.5 years in days 
Ovchepolian.averageWeight = 42 // in kg
Ovchepolian.price = 75;

let Sharplaninka = new Animal();

Sharplaninka.name = "Sharplaninka";
Sharplaninka.averageProduction = 90; // milk in kg
Sharplaninka.averageConsumption = 8;
Sharplaninka.origin = "Southern United States ";
Sharplaninka.content = "Wool 1.3-1.6 kg; 6.5% fat; Fertility 110%;"
Sharplaninka.predictedLifetime = 10950 // 30 years in days 
Sharplaninka.averageWeight = 40 // in kg
Sharplaninka.price = 85;

const Sheep: Animal[] = [Karakachan, Ovchepolian, Sharplaninka];

export default Sheep;