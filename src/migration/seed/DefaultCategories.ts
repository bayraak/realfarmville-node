const baseMediaUrl = 'https://karafil.com/wp-content/uploads/real-farm-ville/';

const DefaultCategories = [
    {
        name: 'Cow',
        logoUrl: baseMediaUrl + 'Inek.svg'
    },
    {
        name: 'Sheep',
        logoUrl: baseMediaUrl + 'Koyun.svg'
    },
    {
        name: 'Horse',
        logoUrl: baseMediaUrl + 'At.svg'
    },
    {
        name: 'Bee Hive',
        logoUrl: baseMediaUrl + 'Kovan.svg'
    },
    {
        name: 'Chicken',
        logoUrl: baseMediaUrl + 'Tavuk.svg'
    }
]

export default DefaultCategories;